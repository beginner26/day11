package comp.Assignment1;

import java.sql.*;
import java.util.Scanner;

public class MySQL {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            // 1. Register Driver Class
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. Creating Connection
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/sonoo","root","admin");
            //here sonoo is database name, root is username and password
            // 3. Create Statement
            Statement stmt=con.createStatement();

            int userInput = 0;
            String nama = "";
            int umur = 0;
            int id = 0;

            while (userInput != 99) {
                System.out.println("MENU");
                System.out.println("1. Show All Record");
                System.out.println("2. Insert Record");
                System.out.println("3. Update Record");
                System.out.println("4. Delete Record");
                System.out.println("99. Exit");

                System.out.print("Pilih Menu: ");
                userInput = input.nextInt();

                switch (userInput) {
                    case 1:
                        System.out.println("Show All Record");
                        ResultSet rs = stmt.executeQuery("select * from emp");
                        while(rs.next())
                            System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
                        break;

                    case 2:
                        System.out.println("Insert Record");
                        System.out.print("Masukan Nama: ");
                        nama = input.next();
                        System.out.print("Masukan Umur: ");
                        umur = input.nextInt();

                        stmt.executeUpdate("insert into emp (name,age) values (\""+nama+"\","+umur+");");
                        break;

                    case 3:
                        System.out.println("Update Record");
                        System.out.print("Masukan ID yang mau diupdate: ");
                        id = input.nextInt();

                        System.out.println("A. Update Nama");
                        System.out.println("B. Update Umur");
                        System.out.println("C. Update Nama & Umur");
                        System.out.print("Pilih: ");
                        String pilih = input.next();

                        if(pilih.equalsIgnoreCase("a")){
                            System.out.print("Update Nama: ");
                            nama = input.next();

                            stmt.executeUpdate("update emp set name = '"+nama+"' where id like '"+id+"'");
                        } else if (pilih.equalsIgnoreCase("b")) {
                            System.out.print("Update Umur: ");
                            umur = input.nextInt();

                            stmt.executeUpdate("update emp set age = "+umur+" where id like '"+id+"'");
                        }else{
                            System.out.print("Update Nama: ");
                            nama = input.next();
                            System.out.print("Update Umur: ");
                            umur = input.nextInt();

                            stmt.executeUpdate("update emp set age = "+umur+", name = '"+nama+"' where id like '"+id+"'");
                        }


                        break;

                    case 4:
                        System.out.println("Delete Record");
                        System.out.print("Masukan ID yang mau didelete: ");
                        id = input.nextInt();

                        stmt.executeUpdate("DELETE FROM emp WHERE id="+id+"");
                        break;
                }
            }

        }catch (Exception e){
            System.out.println(e);
        }
    }
}
