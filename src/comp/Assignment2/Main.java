package comp.Assignment2;

import comp.Assignment2.model.Staff;
import comp.Assignment2.properties.CrunchifyGetPropertyValues;

import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
//        ArrayList<Staff> alStaff = new ArrayList<>();
        // 1. Register Driver Class
        Class.forName("com.mysql.cj.jdbc.Driver");
        Statement stmt = null;
        Connection con = null;

        Scanner input = new Scanner(System.in);
        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
        String ip = "";
        int port = 0;
        String user = "";
        String pass = "";
        String db = "";

        String userInput;
        String sql;

        try {
            System.out.println("Pre-menu");
            System.out.println("A. Import konfigurasi config.properties");
            System.out.println("B. Import data karyawan.txt");

            System.out.println("Menu");
            System.out.println("1. Connect to DB using config.properties database");
            System.out.println("2. Ganti status karyawan (probation, kontrak, tetap, keluar)");
            System.out.println("3. Edit data karyawan (Nama Karyawan)");
            System.out.println("4. Absensi Karyawan");
            System.out.println("5. Cuti Karyawan");
            System.out.println("6. Hitung tunjangan karyawan (tetap)");
            System.out.println("7. Hitung semua total gaji karyawan (!keluar)");
            System.out.println("8. Tampilkan laporan per status");

            System.out.println("Pilih: ");
            userInput = input.next();

            if (userInput.equalsIgnoreCase("a")) {
                ip = config.getPropValues("IP");
                port = Integer.parseInt(config.getPropValues("PORT"));
                user = config.getPropValues("USER");
                pass = config.getPropValues("PASS");
                db = config.getPropValues("NAMA_DB");
                System.out.println("Import konfigurasi config.properties success");
            } else if (userInput.equalsIgnoreCase("b")) {
                // 2. Creating Connection base on config.properties
                con = DriverManager.getConnection("jdbc:mysql://" + ip + ":" + port + "/" + db + "", "" + user + "", "" + pass + "");
                // 3. Create Statement
                stmt = con.createStatement();
                System.out.println("Create connection to database success");

                // Load karyawan.txt to database
                String dataString = "";
                FileReader fr = new FileReader("C:\\tmp\\karyawan.txt");
                int character;
                while ((character = fr.read()) != -1) {
                    dataString = dataString + (char) character;
                }

                String[] splitBySentence = dataString.split("\n");
                sql = "";
                for (int i = 1; i < splitBySentence.length; i++) {
                    String[] splitByWord = splitBySentence[i].trim().split(",");
                    sql = "insert into karyawan(id,nama,gapok,jml_absen,jml_cuti,status) values (?,?,?,?,?,?)";
                    PreparedStatement ps = con.prepareStatement(sql);
                    ps.setInt(1, Integer.parseInt(splitByWord[0]));
                    ps.setString(2, splitByWord[1]);
                    ps.setInt(3, Integer.parseInt(splitByWord[2]));
                    ps.setInt(4, Integer.parseInt(splitByWord[3]));
                    ps.setInt(5, Integer.parseInt(splitByWord[4]));
                    ps.setString(6, splitByWord[5]);
                    ps.executeUpdate();
                }
            } else if (userInput.equalsIgnoreCase("1")) {
                System.out.println("Connect to database");
            } else if (userInput.equalsIgnoreCase("2")) {
                System.out.println("Ganti status karyawan");
                System.out.print("Masukan ID: ");
                int id = input.nextInt();
                System.out.println("Ubah status karyawan ke: ");
                System.out.println("A. Probation");
                System.out.println("B. Kontrak");
                System.out.println("C. Tetap");
                System.out.println("D. Keluar");
                System.out.print("Pilih: ");
                String pilih = input.next();
                String status = "";
                if (pilih.equalsIgnoreCase("A")) {
                    status = "probation";
                } else if (pilih.equalsIgnoreCase("B")) {
                    status = "kontrak";
                } else if (pilih.equalsIgnoreCase("C")) {
                    status = "tetap";
                } else if (pilih.equalsIgnoreCase("D")) {
                    status = "keluar";
                } else {
                    System.out.println("Salah Masukin Pilihan");
                }

                sql = "update karyawan set status = ? where id = ?";
                PreparedStatement ps = con.prepareStatement(sql);
                ps.setString(1, status);
                ps.setInt(2, id);
                ps.executeUpdate();


            } else if (userInput.equalsIgnoreCase("3")) {
                System.out.println("Ganti nama karyawan");
                System.out.print("Masukan ID: ");
                int id = input.nextInt();
                System.out.println("Ubah nama karyawan ke: ");
                System.out.print("Masukan nama baru: ");
                String nama = input.next();

                sql = "update karyawan set nama = ? where id = ?";
                PreparedStatement ps = con.prepareStatement(sql);
                ps.setString(1, nama);
                ps.setInt(2, id);
                ps.executeUpdate();
            } else if (userInput.equalsIgnoreCase("4")) {
                // Load data dari database berdasarkan id
                System.out.println("Absensi Karyawan");
                System.out.print("Masukan ID: ");
                int id = input.nextInt();

                Staff st = new Staff();
                ResultSet rs = stmt.executeQuery("select * from karyawan where id = " + id + " ");
                // Bikin object tersebut dengan model Staff
                while (rs.next()) {
                    System.out.println(rs.getString("nama"));
                    st.setId(rs.getInt("id"));
                    st.setNama(rs.getString("nama"));
                    st.setGapok(rs.getInt("gapok"));
                    st.setAbsen(rs.getInt("jml_absen"));
                    st.setJumcut(rs.getInt("jml_cuti"));
                    st.setStatus(rs.getString("status"));
                }
                // Menambahkan absensi, dimana jika absen lebih dari 22 maka tidak ditambahkan
                st.absensi();

                sql = "update karyawan set jml_absen = ? where id = ?";
                PreparedStatement ps = con.prepareStatement(sql);
                ps.setInt(1, st.getAbsen());
                ps.setInt(2, id);
                ps.executeUpdate();

                System.out.println("absen menjadi: " + st.getAbsen());
            } else if (userInput.equalsIgnoreCase("5")) {
// Load data dari database berdasarkan id
                System.out.println("Cuti Karyawan");
                System.out.print("Masukan ID: ");
                int id = input.nextInt();

                Staff st = new Staff();
                ResultSet rs = stmt.executeQuery("select * from karyawan where id = " + id + " ");
                // Bikin object tersebut dengan model Staff
                while (rs.next()) {
                    System.out.println(rs.getString("nama"));
                    st.setId(rs.getInt("id"));
                    st.setNama(rs.getString("nama"));
                    st.setGapok(rs.getInt("gapok"));
                    st.setAbsen(rs.getInt("jml_absen"));
                    st.setJumcut(rs.getInt("jml_cuti"));
                    st.setStatus(rs.getString("status"));
                }
                // Menambahkan cuti
                st.setJumcut(st.getJumcut() + 1);

                sql = "update karyawan set jml_cuti = ? where id = ?";
                PreparedStatement ps = con.prepareStatement(sql);
                ps.setInt(1, st.getJumcut());
                ps.setInt(2, id);
                ps.executeUpdate();

                System.out.println("cuti menjadi: " + st.getJumcut());
            } else if (userInput.equalsIgnoreCase("6")) {
                System.out.println("Hitung tunjangan karyawan");

                ArrayList<Staff> alStaff = new ArrayList<>();

                ResultSet rs = stmt.executeQuery("select * from karyawan where status like 'tetap' ");
                // Bikin object tersebut dengan model Staff dan masukkan ke arraylist
                while (rs.next()) {
                    Staff st = new Staff();
                    st.setId(rs.getInt("id"));
                    st.setNama(rs.getString("nama"));
                    st.setGapok(rs.getInt("gapok"));
                    st.setAbsen(rs.getInt("jml_absen"));
                    st.setJumcut(rs.getInt("jml_cuti"));
                    st.setStatus(rs.getString("status"));
                    alStaff.add(st);
                }

                for (Staff st : alStaff) {
                    st.hitungTunjMakan();
                    st.hitungTunjTransport();

                    sql = "update karyawan set tunj_makan = ?, tunj_transport = ? where id = ?";
                    PreparedStatement ps = con.prepareStatement(sql);
                    ps.setInt(1, st.getTunjMakan());
                    ps.setInt(2, st.getTunjTransport());
                    ps.setInt(3, st.getId());
                    ps.executeUpdate();
                }
            } else if (userInput.equalsIgnoreCase("7")) {
                System.out.println("Hitung total gaji karyawan");

                ArrayList<Staff> alStaff = new ArrayList<>();

                ResultSet rs = stmt.executeQuery("select * from karyawan where status != 'keluar' ");
                // Bikin object tersebut dengan model Staff dan masukkan ke arraylist
                while (rs.next()) {
                    Staff st = new Staff();
                    st.setId(rs.getInt("id"));
                    st.setNama(rs.getString("nama"));
                    st.setGapok(rs.getInt("gapok"));
                    st.setAbsen(rs.getInt("jml_absen"));
                    st.setJumcut(rs.getInt("jml_cuti"));
                    st.setStatus(rs.getString("status"));
                    st.setTunjMakan(rs.getInt("tunj_makan"));
                    st.setTunjTransport(rs.getInt("tunj_transport"));
                    alStaff.add(st);
                }

                for (Staff st : alStaff) {
                    st.hitungGajiPokok();
                    st.hitungGajiTotal();

                    sql = "update karyawan set total_gaji = ? where id = ?";
                    PreparedStatement ps = con.prepareStatement(sql);
                    ps.setInt(1, st.getTotalGaji());
                    ps.setInt(2, st.getId());
                    ps.executeUpdate();
                }
            } else if (userInput.equalsIgnoreCase("8")) {
                System.out.println("Tampilkan perstatus");
                userInput = input.next();
                ResultSet rs = stmt.executeQuery("select * from karyawan where status != 'keluar' ");
                // Bikin object tersebut dengan model Staff dan masukkan ke arraylist
                while (rs.next()) {
                }
            }


            // Load data dari database berdasarkan id


        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

