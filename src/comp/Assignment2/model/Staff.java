package comp.Assignment2.model;

public class Staff extends Worker {

    int tunjMakan = 220000;
    int tunjTransport = 440000;
    int totalGaji;

//    boolean cutiMelebihi = this.jumcut > 1;

    @Override
    public void hitungGajiPokok() {
        this.gapok = (this.gapok / 22) * this.absen;
    }

    public int getTunjMakan() {
        return tunjMakan;
    }

    public void setTunjMakan(int tunjMakan) {
        this.tunjMakan = tunjMakan;
    }

    public int getTunjTransport() {
        return tunjTransport;
    }

    public void setTunjTransport(int tunjTransport) {
        this.tunjTransport = tunjTransport;
    }

    public int getTotalGaji() {
        return totalGaji;
    }

    public void setTotalGaji(int totalGaji) {
        this.totalGaji = totalGaji;
    }


//    int hitungTunjangan(int tunjangan) {
//        if(cutiMelebihi) {
//            tunjangan = tunjangan - ((tunjangan/22) * jumcut-1);
//        }
//        return tunjangan;
//    }

    public void hitungTunjMakan() {
        if (this.jumcut > 1) {
            this.tunjMakan = this.tunjMakan - ((this.tunjMakan / 22) * this.jumcut - 1);
        }
    }

    public void hitungTunjTransport() {
        if (this.jumcut > 1) {
            this.tunjTransport = this.tunjTransport - ((this.tunjTransport / 22) * this.jumcut - 1);
        }
    }

    public void hitungGajiTotal() {
        this.totalGaji = this.gapok + this.tunjMakan + this.tunjTransport;
    }

}
