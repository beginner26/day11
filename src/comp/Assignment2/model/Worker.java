package comp.Assignment2.model;

abstract class Worker {

    int id;
    String nama;
    int gapok;
    int absen;
    int jumcut;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGapok() {
        return gapok;
    }

    public void setGapok(int gapok) {
        this.gapok = gapok;
    }

    public int getAbsen() {
        return absen;
    }

    public void setAbsen(int absen) {
        this.absen = absen;
    }

    public int getJumcut() {
        return jumcut;
    }

    public void setJumcut(int jumcut) {
        this.jumcut = jumcut;
    }


    // Method
    public void absensi() {
        if (this.absen < 22) this.absen++;
    }

    public void hitungAbsensi() {

    }

    abstract void hitungGajiPokok();

}
